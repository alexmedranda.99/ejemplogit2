from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Color, Ellipse, Line
from random import random
from kivy.uix.button import Button

class programa(Widget):
    def on_touch_down(self, touch):
        color=(random(), random(), random())
        
        with self.canvas:
            Color(*color)
            d=30.
            Ellipse(pos=(touch.x-d/2, touch.y-d/2), size=(d,d))
            touch.ud['line'] = Line(points=(touch.x, touch.y))

    def on_touch_move(self, touch):
        touch.ud['line'].points += [touch.x, touch.y]



class aplicacion(App):
    def build(self):
        parent=Widget()
        self.pintar=programa()
        borrarbtn=Button(text='Borrar')
        borrarbtn.bind(on_release=self.borrarcanvas)
        parent.add_widget(self.pintar)
        parent.add_widget(borrarbtn)
        return parent

    def borrarcanvas(self,obj):
        self.pintar.canvas.clear()





if __name__ == "__main__":
    aplicacion().run()
