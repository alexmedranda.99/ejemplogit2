from kivy.app import App
from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.core.window import Window


#creando lenguaje kv
Builder.load_string("""

<principal>:




    Label:
        id:label1
        text:'Ingrese factorial: '
        font_size:22
        size_hint: 0.5, 1.3
        post_hint: {'center_x':.6, 'y':.5}
        color:  1,.5,0,1

    Label:
        id:label2
        text:''
        font_size:22
        size_hint: 0.9, 0.5
        post_hint: {'center_x':.6, 'y':.5}
        color:  1,.5,0,1

    TextInput:
        id:texto1
        hint_text:'factorial'
        size_hint: None, None
        width:300
        height:50
        pos_hint:{'x':.37,'y':.6}
        font_size:22
        input_filter: 'int'

    BoxLayout:
        size_hint:.9, .4
        orientation: 'horizontal'
        padding:20
        pos_hint:{'center_x':.75, 'y':.35}

        Button:
            text:"Mostrar"
            size_hint:None, None
            width: 100
            height:50
            on_press:root.mostrar(texto1.text)
    
        Button:
            text: "Nuevo"
            size_hint:None, None
            width: 100
            height:50
            on_press:root.borrar()


""")




#creando clase de instrucciones
class principal(FloatLayout):
    def mostrar(self,x):
        f=1
        
        for i in range(1,int(x)+1):
            f*=i
        self.text = x
        self.ids['label2'].text = "El Factorial de "+ x + " es= " + str(f) 



    def borrar(self):
        self.ids['label2'].text = ""
        self.ids['texto1'].text = ""
     
#creando clase app
class TestApp(App):
    
    def build(self):
        Window.clearcolor=0.2,0.2, 0.2, 1.0
        return principal()


if __name__ == "__main__":
    TestApp().run()




    


