from kivy.app import App #Este módulo nos sirve parea crear la aplicación, básicamente lo usaremos siempre.
from kivy.lang import Builder #Este modulo podemos agregar lenguaje KV que nos sirve para crear los gráficos de nuestra aplicación.
from kivy.uix.floatlayout import FloatLayout #Este modulo es un simple contenedor donde agregaremos los botones y etiquetas usando lenguaje KV.
from kivy.core.window import Window #Este modulo lo utilizare simplemente para cambiar el color de fondo de nuestra aplicación.


#Lenguaje KV 
Builder.load_string("""

<texto>:
    Label:
        id:label
        text:"BIENVENIDO"
        font_size:45
        size_hint: 1, 1.5
        post_hint: {'center_x':.5, 'y':.6}
        color: 1,.5,0,1

    Label:
        id:label2
        text:"Recuerda colocar la primera letra en mayuscula"
        font_size:10
        size_hint: 1, 1.3
        post_hint: {'center_x':.5, 'y':.6}
        color: 1.0, 0.0, 0.0, 1.0

    TextInput:
        id:text
        hint_text:"Ingresa tu nombre"
        size_hint: None, None
        width:300
        height:50
        pos_hint:{'center_x':.5,'y':.55}
        font_size:28


    BoxLayout:
        size_hint:.9, .4
        orientation: 'horizontal'
        padding:20
        pos_hint:{'center_x':.75, 'y':.35}

        Button:
            text:"Mostrar"
            size_hint:None, None
            width: 100
            height:50
            on_press:root.mostrar(text.text)
    
        Button:
            text: "Borrar"
            size_hint:None, None
            width: 100
            height:50
            on_press:root.borrar()



""")#Fin del codigo KV





class texto(FloatLayout):
    def mostrar(self,text):
        if(text=='Valentin' or text=='Darwin'):
            self.text = text
            self.ids['label'].text = text+' es gay'
        elif(text=='Alex' and 'Medranda'):
            self.text=text
            self.ids['label'].text = text + ' es un crack'
        
        else:
            self.text = text
            self.ids['label'].text = text+' Tu nombre da asco XD'

    
    def borrar(self):
        self.ids['label'].text = ""
        self.ids['text'].text = ""



class TestApp(App):
    def build(self):
        Window.clearcolor=0,0,0,1
        return texto()

    

if __name__ == "__main__":
    TestApp().run()


